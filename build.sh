#!/bin/sh

# build.sh $VERSION

set -ex

export VERSION="${1:?}"
curl -o src.tar.bz2 "https://busybox.net/downloads/busybox-${VERSION}.tar.bz2"
tar xjf src.tar.bz2
ls
cd busyb*/
ls

make defconfig
sed -i 's/# CONFIG_STATIC is not set/CONFIG_STATIC=y/' .config
make
