#!/bin/sh

curl -s https://busybox.net/downloads/ | grep -o 'busybox-[[:digit:]]*\.[[:digit:]]*\.[[:digit:]]*\.tar.bz2' | uniq | tail -n 1 | grep -o '[[:digit:]]*\.[[:digit:]]*\.[[:digit:]]*'
