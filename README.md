# build_busybox

This repo builds a (single, static) busybox binary from source (from upstream tarballs).
The main purpose here is actually to let me test a build script that self-updates to track the latest version available.


# About the self-updating parts

The idea is this: Have a scheduled job run (here, daily) that can check what
version we're buiilding, and what the latest version is from upstream. If
they're different, make a new branch with our version bumped to match. Since we
also set up CI to run on branches, this results in us automatically attempting
to build and test the new version. Then a human can come and merge if everything
looks good.

Notable details:

* We set a variable, here `DO_A_BUMP=yes`, in the scheduled pipeline config;
  this lets us easily avoid running the version bump logic in regular pipelines.
* In this version, we avoid doing anything if there's already a branch for the
  new version.
* TODO: This version just makes a branch, but it does not do a MR itself.

